package com.ruoyi.web.controller.monitor;

import com.ruoyi.common.core.controller.BaseController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.web.domain.Server;
import reactor.core.publisher.Mono;

/**
 * 服务器监控
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {
	@PreAuthorize("@ss.hasPermi('monitor:server:list')")
	@GetMapping()
	public Mono<AjaxResult> getInfo() {
		return startMono(() -> {

			Server server = new Server();
			try {
				server.copyTo();
			} catch (Exception e) {
				return Mono.error(e);
			}
			return AjaxResult.successMono(server);
		});
	}
}
