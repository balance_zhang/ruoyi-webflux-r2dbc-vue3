package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.biz.IService;
import com.ruoyi.system.domain.SysNotice;

/**
 * 公告 服务层
 *
 * @author ruoyi
 */
public interface ISysNoticeService extends IService<SysNotice, Long> {

}
